module.exports = {
  css: {
    loaderOptions: {
      sass: {
        data: '@import "@/scss/sb-admin-2.scss";'
      }
    },
    sourceMap: true
  },

  publicPath: undefined,
  outputDir: undefined,
  assetsDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: undefined,
  parallel: undefined
};