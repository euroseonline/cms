const mysql = require('mysql2')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const crypto = require('crypto');
const jwtSign = require('jsonwebtoken')
const http = require('http')
const net = require('net')
const config = require('./config')
const customenv = require('custom-env')
const fs = require('fs')
const multer = require('multer')
const path = require('path')
const app = express()
const port = config.port

customenv.env(process.env.NODE_ENV)

const upload = multer({
storage: multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, process.env.VUE_APP_MULTER_DEST_PATH)
    },
    filename: (req, file, cb) => {
        // randomBytes function will generate a random name
        let customFileName = crypto.randomBytes(18).toString('hex')
        // get file extension from original file name
        let fileExtension = file.originalname.split('.')[1]
        cb(null, customFileName + '.' + fileExtension)
    }
  })
})

app.use(cors());
app.options('*', cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const pool = mysql.createPool(config.db);
const promisePool = pool.promise();

function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex');
}

const isReachable = path =>
  new Promise(resolve => {
    const [host, port] = path.split(':')
    const socket = new net.Socket()
    const onError = () => {
      socket.destroy()
      resolve(false)
    }

    socket.setTimeout(1000)
    socket.on('error', onError)
    socket.on('timeout', onError)

    socket.connect(
      port || 80,
      host,
      () => {
        socket.end()
        resolve(true)
      },
    )
  })

  const checkAuth = (req, res, next) => {
    // console.log(req.headers.authorization)
      jwtSign.verify(req.headers.authorization, config.jwt.secret, async (err, decoded) => {
          if(err) {
            return res.send({invalidToken: true})
          } else {
            req.user = decoded.user[0] /* Set decoded token [username] on req.user */
            next();
          }
      });
  }


/* Fetchin news items for the homepage */
app.post('/login', async (req, res) => {
  const [username] = await promisePool.execute('SELECT username FROM accounts WHERE username=?', [req.body.username])
  const [password] = await promisePool.execute('SELECT password FROM accounts WHERE password=?', [md5(req.body.password)])
  const [accesslevel] = await promisePool.execute('SELECT accesslevel FROM accounts WHERE username=?', [req.body.username])

  if(username.length >= 1 && password.length >= 1 && accesslevel[0].accesslevel >= 500) {
    const token = jwtSign.sign({ user: [req.body.username] }, config.jwt.secret);
    res.send({token: token})
  } else {
    return res.send({error: "This account does not exist or is not a GM"})
  }
});

app.get('/fetchNews', checkAuth, async (req, res) => {
  const [newsItems] = await promisePool.query('SELECT * FROM news ORDER BY date DESC');
  res.send(newsItems)
  // console.log('Fetching... News items')
});

app.get('/fetchClans', checkAuth, async (req, res) => {
  const [clans] = await promisePool.query('SELECT * FROM list_clan ORDER BY id ASC');
  res.send(clans)
});

app.get('/fetchChars', checkAuth, async (req, res) => {
  const [chars] = await promisePool.query('SELECT * FROM characters ORDER BY id ASC');
  res.send(chars)
});

app.get('/fetchAccounts', checkAuth, async (req, res) => {
  const [accounts] = await promisePool.query('SELECT * FROM accounts ORDER BY id ASC');
  res.send(accounts)
});

app.get('/dashboard-servers', checkAuth, async (req, res) => {
  const hosts = config.pingHosts
  const status = await Promise.all(hosts.map(isReachable))
  const response = status.map((r, i) => ({ host: hosts[i], isAlive: r }))
  res.send(response)
  res.end()
});

app.post('/fetch-edit-news', checkAuth, async (req,res) => {
    const [fetchData] = await promisePool.execute('SELECT * FROM news WHERE id =?', [req.body.id]);
    res.send({fetchData})
});

app.post('/fetch-edit-clans', checkAuth, async (req,res) => {
    const [fetchClanData] = await promisePool.execute('SELECT * FROM list_clan WHERE id =?', [req.body.id]);
    res.send({fetchClanData})
});

app.post('/fetch-edit-chars', checkAuth, async (req,res) => {
    const [fetchCharData] = await promisePool.execute('SELECT * FROM characters WHERE id =?', [req.body.id]);
    res.send({fetchCharData})
});

app.post('/fetch-edit-accounts', checkAuth, async (req,res) => {
    const [fetchAccountData] = await promisePool.execute('SELECT * FROM accounts WHERE id =?', [req.body.id]);
    res.send({fetchAccountData})
});

app.post('/removeNewsItem', checkAuth, async (req, res) => {
  let checkNewsId = 'DELETE FROM news WHERE id= ?';
  let thumbnailName = 'SELECT thumbnail FROM news WHERE id =?'
  const [fetchThumbnail] = await promisePool.execute(thumbnailName, [req.body.id])
  const [removeNewsItem] = await promisePool.execute(checkNewsId, [req.body.id])
  fs.unlink(path.join(process.env.VUE_APP_MULTER_DEST_PATH, '/', fetchThumbnail[0].thumbnail), err => {
    if(err)
      console.log(err)
  })
  res.send({success: true})
});

app.post('/removeClan', checkAuth, async (req, res) => {
  let checkClanId = 'DELETE FROM list_clan WHERE id= ?';
  const [removeClan] = await promisePool.execute(checkClanId, [req.body.id])
  res.send({success: true})
});

app.post('/removeChar', checkAuth, async (req, res) => {
  let checkCharId = 'DELETE FROM characters WHERE id= ?';
  const [removeChar] = await promisePool.execute(checkCharId, [req.body.id])
  res.send({success: true})
});

app.post('/removeAccount', checkAuth, async (req, res) => {
  let checkAccountId = 'DELETE FROM accounts WHERE id= ?';
  const [removeAccount] = await promisePool.execute(checkAccountId, [req.body.id])
  res.send({success: true})
});

/* Uploading image and saving it a directory, then pass the image.path
   back to the front-end so we can re-use this path when saving a newsItem into the database */
app.post('/upload-image', [checkAuth, upload.single('image')], async (req, res) => {
  res.send({imgPath: req.file.filename})
});

app.post('/saveNewsItem', checkAuth, async (req, res) => {
  const user = req.user;
  const username = user.charAt(0).toUpperCase() + user.slice(1); /* Force username to Capitalize */

  /* If headers.create = true, means the user is creating a new mail
     so we will INSERT with Auto Increment ID
     else the user is updating an existing news item */

  if(req.headers.create === 'true') {
    const [createNews] = await promisePool.execute('INSERT INTO news (title, content, author, date, type, thumbnail, published) VALUES(?, ?, ?, DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 HOUR), "%m/%d/%Y  %h:%i %p"), ?, ?, ?)', [req.body.title, req.body.content, username, req.body.type, req.body.thumbnail, req.body.published])
    res.send({success: true})
  } else {
    const [saveNews] = await promisePool.execute('UPDATE news SET title=?, content=?, author=?, date=DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 HOUR), "%m/%d/%Y  %h:%i %p"), type=?, thumbnail=?, published=? WHERE id=?', [req.body.title, req.body.content, username, req.body.type, req.body.thumbnail, req.body.published, req.body.id])
    res.send({success: true})
  }
});

app.post('/saveClan', checkAuth, async (req, res) => {
  const [saveNews] = await promisePool.execute('UPDATE list_clan SET name=?, slogan=?, news=?, grade=?, cp=?, WHERE id=?', [req.body.name, req.body.slogan, req.body.news, req.body.grade, req.body.cp, req.body.id])
  res.send({success: true})
});

app.post('/saveChar', checkAuth, async (req, res) => {
  const [saveChar] = await promisePool.execute('UPDATE characters SET account_name=?, char_name=?, level=?, exp=?, sex=?, zuly=?, skillp=?, statp=?, curHp=?, curMp=?, str=?, con=?, dex=?, _int=?, cha=?, sen=? WHERE id=?', [req.body.account, req.body.char, req.body.level, req.body.exp, req.body.sex, req.body.zuly, req.body.skillp, req.body.statp, req.body.hp, req.body.mp, req.body.str, req.body.con, req.body.dex, req.body.int, req.body.cha, req.body.sen, req.body.id])
  res.send({success: true})
});

app.post('/saveAccount', checkAuth, async (req, res) => {
  if(req.headers.passwordaltered === 'true') {
    const [saveAccount] = await promisePool.execute('UPDATE accounts SET username=?, password=?, accesslevel=?, email=?, active=?, premium=?, itemmall_points=? WHERE id=?', [req.body.username, md5(req.body.password), req.body.accesslevel, req.body.email, req.body.active, req.body.premium, req.body.itemmall_points, req.body.id])
    res.send({success: true})
  } else {
    const [saveAccountNoPassword] = await promisePool.execute('UPDATE accounts SET username=?, accesslevel=?, email=?, active=?, premium=?, itemmall_points=? WHERE id=?', [req.body.username, req.body.accesslevel, req.body.email, req.body.active, req.body.premium, req.body.itemmall_points, req.body.id])
    res.send({success: true})
  }
});

app.listen(port, () => console.log(`Server running on ${port}!`));