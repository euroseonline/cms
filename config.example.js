module.exports = {
  port: 3000,
  db: {
    host     : 'db_hostname',
    user     : 'db_user',
    password : 'db_pass',
    database : 'db',
  },
  jwt: {
    secret: 'Place your secret key here'
  },
  pingHosts: ['Rose Website URL', 'IP:29000', 'IP:29100', 'IP:29200']
}