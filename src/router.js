import Vue from 'vue'
import Router from 'vue-router'
import Store from '@/store/store'
import Home from './views/Home.vue'
import Dashboard from './views/Dashboard.vue'
import News from './views/News.vue'
import EditNews from './views/EditNews.vue'
import Clans from './views/Clans.vue'
import EditClans from './views/EditClans.vue'
import Characters from './views/Characters.vue'
import EditCharacters from './views/EditCharacters.vue'
import Accounts from './views/Accounts.vue'
import EditAccounts from './views/EditAccounts.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/news',
      name: 'news',
      component: News
    },
    {
      path: '/edit-news/:itemId',
      name: 'edit',
      component: EditNews,
    },
    {
      path: '/create-news/',
      name: 'createNews',
      component: EditNews,
    },
    {
      path: '/clans',
      name: 'clans',
      component: Clans,
    },
    {
      path: '/edit-clans/:itemId',
      name: 'edit-clans',
      component: EditClans
    },
    {
      path: '/characters',
      name: 'characters',
      component: Characters,
    },
    {
      path: '/edit-characters/:itemId',
      name: 'edit-char',
      component: EditCharacters
    },
    {
      path: '/accounts',
      name: 'accounts',
      component: Accounts,
    },
    {
      path: '/edit-accounts/:itemId',
      name: 'edit-accounts',
      component: EditAccounts
    }
  ]
})

function getUser() {
  const token = localStorage.getItem('token');
  Store.commit('SET_TOKEN', token)

  if(!token) {
    router.push('/')
  }
}

getUser()

export default router