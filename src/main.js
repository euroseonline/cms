import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {mapState} from 'vuex'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.use(VueAxios, axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 10000,
}))

library.add(fas,fab)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  computed: mapState({
      user: state => state.user,
    }),
    watch: {
      user: {
        immediate: true,
        handler() {
          // console.log('set headers')
          this.$http.defaults.headers.common['Authorization'] = this.user
        },
      },
    },
  render: h => h(App)
}).$mount('#app')
