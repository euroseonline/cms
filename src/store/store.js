import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  token: null,
}

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  }
}

const store = new Vuex.Store({
  state,
  mutations,
})

export default store